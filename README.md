# kbide

node application สำหรับ kidbright ide

ความต้องการ
- node.js 8
- python
- pyserial

วิธีติดตั้ง
```
git clone https://gitlab.com/kidbright/kbide --recursive
cd kbide
npm run build
```

การรันโปรแกรมใช้คำสั่ง
```
npm start
```
